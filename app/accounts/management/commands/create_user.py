import json

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
       Create a superuser if none exist
       Example:
           manage.py create_user --user=username --password=changeme
     """

    def add_arguments(self, parser):
        parser.add_argument(
            '--username', dest='username', required=True,
            help='Specifies the username for the user',
        )
        parser.add_argument(
            '--password', dest='password', required=True,
            help='Specifies the password for the user',
        )
        parser.add_argument(
            '--email', dest='email', default=None,
            help='Specifies the email for the user',
        )
        parser.add_argument(
            '--superuser', dest='superuser', action='store_true', default=False,
            help='Specifies if user is superuser',
        )
        parser.add_argument(
            '--staff', dest='staff', action='store_true', default=False,
            help='Specifies if user is staff',
        )

    def handle(self, *args, **options):
        User = get_user_model()

        username = options["username"]
        password = options["password"]
        email = options["email"]
        superuser = options["superuser"]
        staff = options["staff"]

        if User.objects.filter(username=username).count():
            self.stdout.write(f'User {username} already exists')
            return

        User.objects.create_user(
            username=username,
            password=password,
            email=email,
            is_superuser=superuser,
            is_staff=staff
        )

        self.stdout.write(f'User "{username}" was created')
