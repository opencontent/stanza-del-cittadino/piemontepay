from django.contrib import admin

# Register your models here.

from .models import PosizioneDebitoria, ListaDiCarico, EnteCreditore
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
from nested_admin import NestedStackedInline, NestedModelAdmin, NestedTabularInline



class PosizioneDebitoriaInline(NestedStackedInline):
    model = PosizioneDebitoria
    readonly_fields = ['iuv', 'codice_avviso', 'codice_esito', 'descrizione_esito']
    extra = 1

class EnteCreditoreAdmin(admin.ModelAdmin):
    list_display = ['denominazione', 'cf']
    search_fields = ['denominazione', 'cf']

class ListaDiCaricoAdmin(NestedModelAdmin):
    list_display = ['id_messaggio', 'ente_creditore', 'codice_versamento', 'importo_totale']
    inlines = [PosizioneDebitoriaInline]
    search_fields = ['denominazione', 'codice_versamento']



class PosizioneDebitoriaAdmin(admin.ModelAdmin, DynamicArrayMixin):
    list_display = ['id_posizione_debitoria', 'causale', 'iuv', 'codice_esito',
                    'identificativo_univoco_fiscale']
    search_fields = ['causale', 'identificativo_univoco_fiscale', 'iuv', 'id_posizione_debitoria']
    readonly_fields = ['iuv', 'codice_avviso', 'codice_esito', 'descrizione_esito']


    def causale_posizione_debitoria(self, obj):
        return obj.posizione_debitoria.causale


admin.site.register(ListaDiCarico, ListaDiCaricoAdmin)
admin.site.register(PosizioneDebitoria, PosizioneDebitoriaAdmin)
admin.site.register(EnteCreditore, EnteCreditoreAdmin)

