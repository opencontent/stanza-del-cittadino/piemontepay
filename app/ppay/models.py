import uuid

from django.core import validators
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django_json_field_schema_validator.validators import JSONFieldSchemaValidator

# Create your models here.

schema_riferimenti_pagamento = {
    '$schema': f'http://json-schema.org/draft-07/schema#',
    'type': 'array',
    "items": {
        "type": "object",
        "properties": {
            "nome": {"type": "string", "maxLength": 70},
            "valore": {"type": "string", "maxLength": 70},
        },
        "required": ["nome", "valore"]
    },
    "maxItems": 5,
}

schema_componenti_importo = {
    '$schema': f'http://json-schema.org/draft-07/schema#',
    'type': 'array',
    "items": {
        "type": "object",
        "properties": {
            "importo": {"type": "number", "minValue": 0.00, "maxValue": 99999999.99, "multipleOf": 0.01},
            "causale_descrittiva": {"type": "string", "maxLength": 80},
            "dati_specifici_riscossione": {"type": "string", "maxLength": 140, "pattern": "^[0129]{1}/\S{3,138}$"},
            "anno_accertamento": {"type": "integer", "minValue": 2000, "maxValue": 2100},
            "numero_accertamento": {"type": "string", "maxLength": 35},
        },
        "required": ["importo", "causale_descrittiva"]
    },
    "maxItems": 5,
}


class EnteCreditore(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True,
                          verbose_name="Identificativo ente creditore")
    denominazione = models.CharField(max_length=70, verbose_name="Denominazione Ente creditore")
    cf = models.CharField(max_length=35, verbose_name="CF Ente creditore")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Data di creazione")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Data ultimo aggiornamento")

    class Meta:
        verbose_name = "Ente creditore"
        verbose_name_plural = "Enti creditori"
        ordering = ['created_at']

    def __str__(self):
        return f"{self.denominazione} [{self.cf}]"


class ListaDiCarico(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True,
                          verbose_name="Identificativo lista di carico")
    id_messaggio = models.CharField(max_length=35, verbose_name="Identificativo messaggio", unique=True)
    ente_creditore = models.ForeignKey("ppay.EnteCreditore", on_delete=models.PROTECT, verbose_name="Ente creditore")
    codice_versamento = models.CharField(max_length=4, verbose_name="Codice versamento")
    numero_posizioni_debitorie = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(999999)], verbose_name="Numero posizioni debitorie")
    importo_totale = models.DecimalField(validators=[MinValueValidator(0.00), MaxValueValidator(99999999.99)],
                                         max_digits=10,
                                         decimal_places=2, verbose_name="Importo totale lista di carico")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Data di creazione")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Data ultimo aggiornamento")

    class Meta:
        verbose_name = "Lista di carico"
        verbose_name_plural = "Liste di carico"
        ordering = ['created_at']

    def __str__(self):
        return self.id_messaggio


class PosizioneDebitoria(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True,
                          verbose_name="Identificativo Posizione debitoria da inserire")
    id_posizione_debitoria = models.CharField(max_length=50, unique=True,
                                              verbose_name="Identificativo della posizione debitoria")
    anno_riferimento = models.PositiveIntegerField(validators=[MinValueValidator(2000), MaxValueValidator(2100)],
                                                   null=True, blank=True, verbose_name="Anno di riferimento")
    totale = models.PositiveIntegerField(validators=[MinValueValidator(1)], verbose_name="Importo totale")
    scadenza = models.DateField(null=True, verbose_name="Data di scadenza")
    inizio_validita = models.DateField(null=True, blank=True, verbose_name="Data di inizio validità")
    fine_validita = models.DateField(null=True, blank=True, verbose_name="Data di fine validità")
    causale = models.CharField(max_length=100, verbose_name="Descrizione causale versamento")
    rata = models.CharField(max_length=16, null=True, blank=True, verbose_name="Descrizione rata")
    componenti_importo = models.JSONField(validators=[JSONFieldSchemaValidator(schema_componenti_importo)],
                                          null=True, blank=True,
                                          verbose_name="Componenti importo")
    riferimenti_pagamento = models.JSONField(validators=[JSONFieldSchemaValidator(schema_riferimenti_pagamento)],
                                             null=True, blank=True,
                                             verbose_name="Riferimenti pagamento")
    note = models.CharField(max_length=100, null=True, blank=True, verbose_name="Note per il pagatore")
    soggetto_pagatore_name = models.CharField(max_length=70, null=True, blank=True,
                                              verbose_name="Nome soggetto pagatore (persona fisica)")
    soggetto_pagatore_surname = models.CharField(max_length=70, null=True, blank=True,
                                                 verbose_name="Cognome soggetto pagatore (persona fisica)")
    ragione_sociale = models.CharField(max_length=70, null=True, blank=True,
                                       verbose_name="Ragione sociale (persona giuridica)")
    identificativo_univoco_fiscale = models.CharField(max_length=35,
                                                      verbose_name="Identificativo univoco fiscale soggetto pagatore")
    indirizzo = models.CharField(max_length=70, null=True, blank=True, verbose_name="Indirizzo soggetto oagatore")
    civico = models.CharField(max_length=16, null=True, blank=True, verbose_name="Numero civico soggetto oagatore")
    cap = models.CharField(max_length=16, null=True, blank=True, verbose_name="CAP soggetto oagatore")
    localita = models.CharField(max_length=35, null=True, blank=True, verbose_name="Località soggetto oagatore")
    provincia = models.CharField(max_length=35, null=True, blank=True, verbose_name="Provincia soggetto oagatore")
    nazione = models.CharField(max_length=2, validators=[validators.RegexValidator(r'[A-Z]{2}')], null=True, blank=True,
                               verbose_name="Nazione soggetto oagatore")
    email = models.EmailField(max_length=256, validators=[validators.RegexValidator(r'.+@.+')], null=True, blank=True,
                              verbose_name="Email soggetto oagatore")
    lista_di_carico = models.ForeignKey("ppay.ListaDiCarico", on_delete=models.PROTECT, verbose_name="Lista di carico")
    iuv = models.CharField(max_length=35, null=True, blank=True, verbose_name="Identificativo univoco versamento")
    codice_avviso = models.CharField(max_length=35, null=True, blank=True, verbose_name="Codice avviso di pagamento")
    codice_esito = models.CharField(max_length=3, null=True, blank=True,
                                    validators=[validators.RegexValidator(r'[012][0-9]{2}')],
                                    verbose_name="Codice esito inserimento")
    descrizione_esito = models.CharField(max_length=200, null=True, blank=True,
                                         verbose_name="Descrizione esito inserimento")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Data di creazione")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Data ultimo aggiornamento")

    class Meta:
        verbose_name = "Posizione debitoria"
        verbose_name_plural = "Posizioni debitorie"
        ordering = ['created_at']

    def __str__(self):
        return self.causale
