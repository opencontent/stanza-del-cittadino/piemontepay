from django.urls import path, include
from rest_framework import routers
from ppay.views import EsitoInserimento, index, PosizioneDebitoriaViewSet

# Routers provide an easy way of automatically determining the URL conf.

router = routers.DefaultRouter()
router.register(r'posizioni-debitorie', PosizioneDebitoriaViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('importaDovuto/', index, name='index'),
    path('esitoInserimento/', EsitoInserimento.as_view(), name='esito'),
]
