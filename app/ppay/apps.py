from django.apps import AppConfig


class PpayConfig(AppConfig):
    name = 'ppay'

    def ready(self):
        import ppay.signals
